package com.idlegames;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class LoginScreen extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_screen);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_login_screen, menu);
		return true;
	}

	public void login(View view) {
		EditText editTextName = (EditText) findViewById(R.id.editText1);
		String name = editTextName.getText().toString();

		EditText editTextPassword = (EditText) findViewById(R.id.editText2);
		String password = editTextPassword.getText().toString();

		ConnectionConfiguration cc = new ConnectionConfiguration(
				"ankurs-macbook-pro.local", 5222, "ankurs-macbook-pro.local");
		HomeScreen.connection = new XMPPConnection(cc);

		try {
			HomeScreen.connection.connect();
			// You have to put this code before you login
			SASLAuthentication.supportSASLMechanism("PLAIN", 0);

			// You have to specify your Jabber ID addres WITHOUT @jabber.org at
			// the end
			HomeScreen.connection.login(name, password);

			// See if you are authenticated
			System.out.println(HomeScreen.connection.isAuthenticated());
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
