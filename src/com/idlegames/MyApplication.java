package com.idlegames;

import org.jivesoftware.smack.XMPPConnection;




public class MyApplication  {
	
	public static XMPPConnection connection;;

	public XMPPConnection getConnection() {
		return connection;
	}

	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

}
