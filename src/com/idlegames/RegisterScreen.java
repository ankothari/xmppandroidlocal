package com.idlegames;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class RegisterScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_register_screen, menu);
        return true;
    }
    public void register(View view){
    	AccountManager am = new AccountManager(HomeScreen.connection);
    	EditText editTextName = (EditText) findViewById(R.id.editText1);
		String name = editTextName.getText().toString();

		EditText editTextPassword = (EditText) findViewById(R.id.editText2);
		String password = editTextPassword.getText().toString();

		ConnectionConfiguration cc = new ConnectionConfiguration(
				"ankurs-macbook-pro.local", 5222, "ankurs-macbook-pro.local");
		HomeScreen.connection = new XMPPConnection(cc);
    	try {
			am.createAccount(name, password);
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
