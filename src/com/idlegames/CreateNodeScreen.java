package com.idlegames;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class CreateNodeScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_node_screen);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_create_node_screen, menu);
        return true;
    }
}
